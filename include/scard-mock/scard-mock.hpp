#include <string>
#include <set>
#include <vector>
#include <stdexcept>

class SCardMockError : public std::runtime_error
{
public:
    using std::runtime_error::runtime_error;
};

class SCardMock
{
public:
    typedef std::vector<unsigned char> byte_vector;
    typedef std::vector<byte_vector> Script;
    typedef std::string string_t;

    bool wasCalled(const std::string& function)
    {
        return _recordedCalls.find(function) != _recordedCalls.end();
    }

    void call(const std::string& function) { _recordedCalls.insert(function); }

    byte_vector responseForCommand(const byte_vector& command)
    {
        // restart script if at end
        if (_stepCount >= _script.size())
            _stepCount = 0;

        if (_script[_stepCount] != command)
            throw SCardMockError("Command not in script");

        auto& response = _script[++_stepCount];
        ++_stepCount;
        return response;
    }

    static SCardMock& instance()
    {
        static SCardMock self;
        return self;
    }

    static void setScript(const Script& script);

    static const byte_vector& atr() { return instance()._atr; }
    static void setAtr(const byte_vector& atr) { instance()._atr = atr; }

    static const byte_vector DEFAULT_CARD_ATR;
    static const std::vector<string_t> DEFAULT_READER_NAMES;

    static const byte_vector DEFAULT_COMMAND_APDU;
    static const byte_vector DEFAULT_RESPONSE_APDU;
    static const Script DEFAULT_SCRIPT;

private:
    SCardMock();
    ~SCardMock() = default;

    // The rule of five (C++ Core guidelines C.21).
    SCardMock(const SCardMock&) = delete;
    SCardMock& operator=(const SCardMock&) = delete;
    SCardMock(const SCardMock&&) = delete;
    SCardMock& operator=(const SCardMock&&) = delete;

    std::set<std::string> _recordedCalls;
    byte_vector _atr;
    Script _script;
    size_t _stepCount;
};
