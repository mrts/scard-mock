# pcsc-mock

C++ library for mocking the PC/SC API functions.

## Building

    apt install build-essential pkg-config cmake libgtest-dev valgrind libpcsclite-dev
    sudo bash -c 'cd /usr/src/googletest && cmake . && cmake --build . --target install'

    cd build
    cmake .. # optionally with -DCMAKE_BUILD_TYPE=Debug
    cmake --build . # optionally with VERBOSE=1

## Testing

Build as described above, then inside `build` directory:

    ctest # or 'valgrind --leak-check=full ctest'

## Development guidelines

- Format code with `scripts/clang-format.sh` before committing
