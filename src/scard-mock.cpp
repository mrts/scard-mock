#include <scard-mock/scard-mock.hpp>

#include <winscard.h>

#include <iostream>
#include <cstring>

using namespace std;

const SCardMock::byte_vector SCardMock::DEFAULT_CARD_ATR {0x1, 0x2, 0x3, 0x4};
const vector<SCardMock::string_t> SCardMock::DEFAULT_READER_NAMES {"SCardMock-reader"};

const SCardMock::byte_vector SCardMock::DEFAULT_COMMAND_APDU {0x2, 0x1, 0x3, 0x4};
const SCardMock::byte_vector SCardMock::DEFAULT_RESPONSE_APDU {0x90, 0x3};

const SCardMock::Script SCardMock::DEFAULT_SCRIPT {DEFAULT_COMMAND_APDU, DEFAULT_RESPONSE_APDU};

SCardMock::SCardMock() :
    _recordedCalls(), _atr(DEFAULT_CARD_ATR), _script(DEFAULT_SCRIPT), _stepCount(0)
{
}

void SCardMock::setScript(const Script& script)
{
    if (script.size() % 2 != 0)
        throw SCardMockError("Script must contain even number of elements");

    auto& self = instance();
    self._script = script;
    self._stepCount = 0;
}

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4273)
__declspec(dllexport) const SCARD_IO_REQUEST g_rgSCardT0Pci {
    0,
};
__declspec(dllexport) const SCARD_IO_REQUEST g_rgSCardT1Pci {
    0,
};
__declspec(dllexport) const SCARD_IO_REQUEST g_rgSCardTRawPci {
    0,
};
#pragma warning(pop)
#endif

#ifdef _MSC_VER
WINSCARDAPI LONG WINAPI SCardEstablishContext(_In_ DWORD, _Reserved_ LPCVOID, _Reserved_ LPCVOID,
                                              _Out_ LPSCARDCONTEXT)
#else
LONG SCardEstablishContext(DWORD, LPCVOID, LPCVOID, LPSCARDCONTEXT)
#endif
{
    SCardMock::instance().call(__FUNCTION__);
    return 0;
}

#ifdef _MSC_VER
WINSCARDAPI LONG WINAPI SCardReleaseContext(SCARDCONTEXT)
#else
LONG SCardReleaseContext(SCARDCONTEXT)
#endif
{
    SCardMock::instance().call(__FUNCTION__);
    return 0;
}

#ifdef _MSC_VER // TODO: multibyte/Unicode API in Windows?
WINSCARDAPI LONG WINAPI SCardListReadersW(SCARDCONTEXT, LPCWSTR, LPWSTR mszReaders,
                                          LPDWORD pcchReaders)
#else
LONG SCardListReaders(SCARDCONTEXT, LPCSTR, LPSTR mszReaders, LPDWORD pcchReaders)
#endif
{
    SCardMock::instance().call(__FUNCTION__);

    if (!pcchReaders)
        return SCARD_E_INVALID_PARAMETER;

    DWORD bufferLength = SCardMock::DEFAULT_READER_NAMES[0].size() + 1;

    if (mszReaders && *pcchReaders < bufferLength)
        return SCARD_E_INSUFFICIENT_BUFFER;

    *pcchReaders = bufferLength;

    if (!mszReaders)
        // if buffer not given, only output buffer length
        return SCARD_S_SUCCESS;

    auto buf = SCardMock::DEFAULT_READER_NAMES[0] + '\0';

    memcpy(mszReaders, buf.c_str(), buf.size() * sizeof(SCardMock::string_t::value_type));

    return SCARD_S_SUCCESS;
}

#ifdef _MSC_VER // TODO: multibyte/Unicode API in Windows?
WINSCARDAPI LONG WINAPI SCardConnectW(_In_ SCARDCONTEXT, _In_ LPCWSTR, _In_ DWORD,
                                      _In_ DWORD requestedProtocol, _Out_ LPSCARDHANDLE cardHandle,
                                      _Out_ LPDWORD protocolOut)
#else
LONG SCardConnect(SCARDCONTEXT, LPCSTR, DWORD, DWORD requestedProtocol, LPSCARDHANDLE cardHandle,
                  LPDWORD protocolOut)
#endif
{
    SCardMock::instance().call(__FUNCTION__);
    *cardHandle = 0x0ff;
    *protocolOut = requestedProtocol;
    return 0;
}

#ifdef _MSC_VER
WINSCARDAPI LONG WINAPI SCardDisconnect(_In_ SCARDHANDLE, _In_ DWORD)
#else
LONG SCardDisconnect(SCARDHANDLE, DWORD)
#endif
{
    SCardMock::instance().call(__FUNCTION__);
    return 0;
}

#ifdef _MSC_VER // TODO: multibyte/Unicode API in Windows?
WINSCARDAPI LONG WINAPI SCardGetStatusChangeW(SCARDCONTEXT, DWORD,
                                              LPSCARD_READERSTATEW rgReaderStates, DWORD cReaders)
#else
LONG SCardGetStatusChange(SCARDCONTEXT, DWORD, LPSCARD_READERSTATE rgReaderStates, DWORD cReaders)
#endif
{
    SCardMock::instance().call(__FUNCTION__);
    const auto& atr = SCardMock::instance().atr();

    if (!rgReaderStates || cReaders != 1)
        return SCARD_E_INVALID_PARAMETER;

    rgReaderStates->szReader = SCardMock::DEFAULT_READER_NAMES[0].c_str();
    size_t atrSize = atr.size();
    memcpy(rgReaderStates->rgbAtr, &atr[0], atrSize);
    rgReaderStates->cbAtr = atrSize;

    return SCARD_S_SUCCESS;
}

#ifdef _MSC_VER
WINSCARDAPI LONG WINAPI SCardBeginTransaction(_In_ SCARDHANDLE)
#else
LONG SCardBeginTransaction(SCARDHANDLE)
#endif
{
    SCardMock::instance().call(__FUNCTION__);
    return 0;
}

#ifdef _MSC_VER
WINSCARDAPI LONG WINAPI SCardEndTransaction(_In_ SCARDHANDLE, _In_ DWORD)
#else
LONG SCardEndTransaction(SCARDHANDLE, DWORD)
#endif
{
    SCardMock::instance().call(__FUNCTION__);
    return 0;
}

#ifdef _MSC_VER
WINSCARDAPI LONG WINAPI SCardTransmit(_In_ SCARDHANDLE, _In_ LPCSCARD_IO_REQUEST,
                                      _In_reads_bytes_(commandBytesLength) LPCBYTE commandBytes,
                                      _In_ DWORD commandBytesLength, _Inout_opt_ LPSCARD_IO_REQUEST,
                                      _Out_writes_bytes_(*responseBytesLength) LPBYTE responseBytes,
                                      _Inout_ LPDWORD responseBytesLength)
#else
LONG SCardTransmit(SCARDHANDLE, LPCSCARD_IO_REQUEST, LPCBYTE commandBytes, DWORD commandBytesLength,
                   LPSCARD_IO_REQUEST, LPBYTE responseBytes, LPDWORD responseBytesLength)
#endif
{
    auto& SCardMock = SCardMock::instance();
    SCardMock.call(__FUNCTION__);

    if (!commandBytes || !responseBytes || commandBytesLength < 1 || *responseBytesLength < 1)
        return SCARD_E_INVALID_PARAMETER;

    SCardMock::byte_vector command {commandBytes, commandBytes + commandBytesLength};
    SCardMock::byte_vector response = SCardMock.responseForCommand(command);

    DWORD responseLenght = response.size();

    if (*responseBytesLength < responseLenght)
        return SCARD_E_INSUFFICIENT_BUFFER;

    *responseBytesLength = responseLenght;

    memcpy(responseBytes, &response[0], responseLenght);

    return SCARD_S_SUCCESS;
}

#ifdef _MSC_VER
WINSCARDAPI LONG WINAPI SCardControl(_In_ SCARDHANDLE, _In_ DWORD,
                                     _In_reads_bytes_(cbInBufferSize) LPCVOID, _In_ DWORD,
                                     _Out_writes_bytes_(cbOutBufferSize) LPVOID, _In_ DWORD,
                                     _Out_ LPDWORD)
#else
LONG SCardControl(SCARDHANDLE, DWORD, LPCVOID, DWORD, LPVOID, DWORD, LPDWORD)
#endif
{
    SCardMock::instance().call(__FUNCTION__);
    return 0;
}

#ifdef _MSC_VER
WINSCARDAPI LONG WINAPI SCardFreeMemory(_In_ SCARDCONTEXT, _In_ LPCVOID)
#else
LONG SCardFreeMemory(SCARDCONTEXT, LPCVOID)
#endif
{
    SCardMock::instance().call(__FUNCTION__);
    return 0;
}
