#include <scard-mock/scard-mock.hpp>

#include "gtest/gtest.h"

#include <winscard.h>

#include <vector>
#include <string>

TEST(scard_mock_test, testScardCalls)
{
    using namespace std;

    auto& scardMock = SCardMock::instance();

    SCARDCONTEXT _context;
    LONG rv = SCardEstablishContext(SCARD_SCOPE_SYSTEM, nullptr, nullptr, &_context);
    EXPECT_FALSE(rv);
    EXPECT_TRUE(scardMock.wasCalled("SCardEstablishContext"));

    vector<SCardMock::string_t::value_type> readerNames;
    readerNames.resize(25);
    DWORD readerNamesLen = readerNames.size();
    rv = SCardListReaders(_context, nullptr,
                          reinterpret_cast<SCardMock::string_t::value_type*>(readerNames.data()),
                          &readerNamesLen);
    EXPECT_FALSE(rv);
    EXPECT_TRUE(scardMock.wasCalled("SCardListReaders"));
    SCardMock::string_t readerName(readerNames.data());
    EXPECT_EQ(readerName, SCardMock::DEFAULT_READER_NAMES[0]);

    std::vector<SCARD_READERSTATE> readerStates {{nullptr,
                                                  nullptr,
                                                  0,
                                                  0,
                                                  0,
                                                  {
                                                      0,
                                                  }}};
    rv = SCardGetStatusChange(_context, 0, &readerStates[0], DWORD(readerStates.size()));
    EXPECT_FALSE(rv);
    EXPECT_TRUE(scardMock.wasCalled("SCardGetStatusChange"));
    readerName = &(readerStates[0].szReader)[0];
    EXPECT_EQ(readerName, SCardMock::DEFAULT_READER_NAMES[0]);

    auto atrBuf = readerStates[0].rgbAtr;
    vector<unsigned char> atr(atrBuf, atrBuf + readerStates[0].cbAtr);
    EXPECT_EQ(atr, SCardMock::DEFAULT_CARD_ATR);

    auto protocol = SCARD_PROTOCOL_T0;
    DWORD protocolOut = SCARD_PROTOCOL_UNDEFINED;
    SCARDHANDLE _card;
    rv = SCardConnect(_context, "", SCARD_SHARE_SHARED, protocol, &_card, &protocolOut);
    EXPECT_FALSE(rv);
    EXPECT_TRUE(scardMock.wasCalled("SCardConnect"));

    rv = SCardBeginTransaction(_card);
    EXPECT_FALSE(rv);
    EXPECT_TRUE(scardMock.wasCalled("SCardBeginTransaction"));

    auto commandBytes = SCardMock::byte_vector {2, 1, 3, 4};
    auto responseBytes = SCardMock::byte_vector(5, 0);
    DWORD responseLength = responseBytes.size();
    SCARD_IO_REQUEST _protocol; // = *SCARD_PCI_T0; <-- non-trivial
    rv = SCardTransmit(_card, &_protocol, commandBytes.data(), commandBytes.size(), nullptr,
                       responseBytes.data(), &responseLength);
    EXPECT_FALSE(rv);
    EXPECT_TRUE(scardMock.wasCalled("SCardTransmit"));

    rv = SCardEndTransaction(_card, SCARD_LEAVE_CARD);
    EXPECT_FALSE(rv);
    EXPECT_TRUE(scardMock.wasCalled("SCardEndTransaction"));

    rv = SCardDisconnect(_card, SCARD_LEAVE_CARD);
    EXPECT_FALSE(rv);
    EXPECT_TRUE(scardMock.wasCalled("SCardDisconnect"));

    rv = SCardReleaseContext(_context);
    EXPECT_FALSE(rv);
    EXPECT_TRUE(scardMock.wasCalled("SCardReleaseContext"));
}
